package gosession

import (
	"crypto/rand"
	"encoding/base64"
	"errors"
	"net/http"
	"net/url"
	"time"
)

type Session interface {
	Get(key interface{}) interface{}
	Set(key, value interface{}) error
	Id() string
	Delete(key interface{}) error
}

type Provider interface {
	Init(sid string) (Session, error)
	Read(sid string) (Session, error)
	Destroy(sid string) error
	GC(maxlifetime time.Duration)
}

var provides = make(map[string]Provider)

func Register(name string, prov Provider) {
	if prov == nil {
		panic("provider nil")
	}
	if _, dup := provides[name]; dup {
		panic("dup provider")
	}
	provides[name] = prov
}

type ManagerInterface interface {
	sessionId() string
	SessionStart(w http.ResponseWriter, r *http.Request) (session Session)
	SessionDestroy(w http.ResponseWriter, r *http.Request)
	GC()
}

type Manager struct {
	cookieName  string //private cookiename
	provider    Provider
	maxlifetime time.Duration
}

func NewManager(name string, cookieName string, maxlifetime time.Duration) (*Manager, error) {
	provider, ok := provides[name]
	if !ok {
		return nil, errors.New("unknow provider")
	}
	return &Manager{cookieName: cookieName, provider: provider, maxlifetime: maxlifetime}, nil
}

func (m *Manager) sessionId() string {
	b := make([]byte, 32)
	_, err := rand.Read(b)
	if err != nil {
		panic("rand read error:" + err.Error())
	}
	return base64.URLEncoding.EncodeToString(b)
}

func (m *Manager) SessionStart(w http.ResponseWriter, r *http.Request) (session Session) {
	c, err := r.Cookie(m.cookieName)
	if err != nil || c.Value == "" {
		sid := m.sessionId()
		session, _ = m.provider.Init(sid)
		cookie := http.Cookie{Name: m.cookieName, Value: url.QueryEscape(sid), Path: "/", HttpOnly: true, MaxAge: int(m.maxlifetime.Seconds())}
		http.SetCookie(w, &cookie)
	} else {
		sid, _ := url.QueryUnescape(c.Value)
		session, _ = m.provider.Read(sid)
	}
	return
}

func (m *Manager) SessionDestroy(w http.ResponseWriter, r *http.Request) {
	cookie, err := r.Cookie(m.cookieName)
	if err != nil || cookie.Value == "" {
		return
	} else {
		m.provider.Destroy(cookie.Value)
		expiration := time.Now()
		cookie := http.Cookie{Name: m.cookieName, Path: "/", HttpOnly: true, Expires: expiration, MaxAge: -1}
		http.SetCookie(w, &cookie)
	}
}

func (m *Manager) GC() {
	m.provider.GC(m.maxlifetime)
	time.AfterFunc(m.maxlifetime, func() { m.GC() })
}
