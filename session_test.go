package gosession

import (
	"testing"
	"time"
)

type MyP struct {
}

func (*MyP) GC(maxlifetime time.Duration) {
	panic("implement me")
}

func (*MyP) Init(sid string) (Session, error) {
	panic("implement me")
}

func (*MyP) Read(sid string) (Session, error) {
	panic("implement me")
}

func (*MyP) Destroy(sid string) error {
	panic("implement me")
}

func TestManager(t *testing.T) {
	Register("test", &MyP{})
	m, _ := NewManager("test", "cname", time.Minute*10)
	t.Log("m sessionid:", m.sessionId())
}
